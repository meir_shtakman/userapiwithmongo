export class HttpError extends Error{
    constructor(public message: string,public statusCode:number){
        super(message);
    }

    toString():string {
        return `Status code: ${this.statusCode}, Message: ${this.message}
            Stack: ${this.stack}
            `;
    }
}

export interface httpResponseMessage{
    status: number;
    message: string;
    data: any;
}

export interface IIndexObject{
    [index:string]:any
}

export interface IUser {
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
}

export type UserDetailsForUpdate = Partial<IUser>;

