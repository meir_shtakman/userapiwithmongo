import Ajv from "ajv";
import {Request,Response,NextFunction } from "express";
import { HttpError } from "../../utilities/types.js";

const ajv = new Ajv();

export default function validateUserObject(req:Request, res:Response, next:NextFunction){
    const data = req.body;
    let valid = true;
    console.log(req.method);
    switch(req.method){
        case "POST":
            valid = ajv.validate(schemaForPost, data);
            break;
        case "PUT":
            valid = ajv.validate(schemaForPut, data);
            break;
        default: 
            break;       
    }
    
    if (!valid){
         console.log(ajv.errors);
         next(new HttpError(String(`${ajv.errors?ajv.errors[0].instancePath:undefined} ${ajv.errors?ajv.errors[0].message:undefined}`),400));
        } else {
            next();
        }

}

const schemaForPost = {
  type: "object",
  properties: {
    first_name: {type: "string" ,pattern: "[a-zA-Z]{2,30}"},
    last_name: {type: "string" ,pattern: "[a-zA-Z]{2,30}"},
    email: {type: "string",pattern: "[a-zA-Z_@.]"},
    //, pattern: "^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$"
    phone: {type: "string", pattern: "[0-9]{8,12}"}
  },
  required: ["first_name","last_name","email","phone"],
  additionalProperties: false
};

const schemaForPut = {
    type: "object",
    properties: {
      first_name: {type: "string" ,pattern: "[a-zA-Z]{2,30}"},
      last_name: {type: "string" ,pattern: "[a-zA-Z]{2,30}"},
      email: {type: "string",pattern: "[a-zA-Z_@.]"},
      //, pattern: "^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$"
      phone: {type: "string", pattern: "[0-9]{8,12}"}
    },
    additionalProperties: false
  };

