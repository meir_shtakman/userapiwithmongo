import { IUser, UserDetailsForUpdate } from "../../utilities/types.js";
import user_model from "./user.model.js";


export async function createUser(userData: IUser) {
    const user = await user_model.create(userData);
    return user;
}

export async function getAllUsers() {
    const users = await user_model.find()
        // .select(`-__v`);
        .select(`-_id 
            first_name 
            last_name 
            email 
            phone`);
    return users;
}

export async function getUsersPagination(pagination: number, limit: number) {
    const users = await user_model.find().skip(pagination > 0 ?
        ((pagination - 1) * limit) : 0)
        .limit(limit)
        .select("-__v");
    return users;
}

export async function getSingleUser(id: string) {
    const user = await user_model.findById(id)
        .select(`-_id 
                first_name 
                last_name 
                email
                phone`);
    return user;
};

export async function updateSingleUser(id: string, userData: UserDetailsForUpdate) {
    const user = await user_model.findByIdAndUpdate(id, userData,
        { new: true, upsert: false });
    return user;
};

export async function deleteUser(id: string) {
    const user = await user_model.findByIdAndRemove(id);
    return user;
};