import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name: {
        type: String, validate: {
            validator: function (v: string) {
                return /[a-zA-Z]{2,30}/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid first name!`
        },
        required: [true, "User first name required"]
    },
    last_name: {
        type: String, validate: {
            validator: function (v: string) {
                return /[a-zA-Z]{2,30}/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid last name!`
        },
        required: [true, "User last name required"]
    },
    email: {
        type: String, validate: {
            validator: function (v: string) {
                return /[a-zA-Z_@.]/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid email!`
        },
        required: [true, "User email required"]
    },
    phone: {
        type: String, validate: {
            validator: function (v: string) {
                return /[0-9]{8,12}/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid phone number!`
        },
        required: [true, "User phone required"]
    },
});

export default model("user", UserSchema);