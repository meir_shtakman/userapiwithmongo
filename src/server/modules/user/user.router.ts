/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express from "express";
import { Request, Response } from "express";
import validate from "./validate.users.input.js";
import { createUser, deleteUser, getAllUsers, getSingleUser, getUsersPagination, updateSingleUser } from "./user.logic.js";
import { HttpError } from "../../utilities/types.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
router.post("/", validate, raw(async (req, res) => {
  const user = await createUser(req.body);
  res.status(200).json(user);
}));


// GET ALL USERS
router.get("/", raw(async (req, res) => {
  const users = await getAllUsers();
  res.status(200).json(users);
})
);

router.get("/:pagination/:limit", raw(async (req, res) => {
  const limit: number = parseInt(req.params.limit);
  const pagination: number = parseInt(req.params.pagination);

  const users = getUsersPagination(pagination, limit);

  res.status(200).json(users);
})
);


// GETS A SINGLE USER
router.get("/:id", raw(async (req, res) => {
  const user = await getSingleUser(req.params.id);
  if (!user) {
    throw new HttpError("user not found", 400);
  }
  res.status(200).json(user);
}));
// UPDATES A SINGLE USER
router.put("/:id", validate, raw(async (req, res) => {
  const user = updateSingleUser(req.params.id, req.body);
  res.status(200).json(user);
})
);


// DELETES A USER
router.delete("/:id", raw(async (req: Request, res: Response) => {
  const user = await deleteUser(req.params.id);
  if (!user) throw new HttpError("No user found.", 400);
  res.status(200).json(user);
})
);

export default router;
